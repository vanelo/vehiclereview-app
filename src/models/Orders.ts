export class Orders { 
    id: string; 
    products: object; 
    card: number;
    status: string;
    moneyTransaction: object;
    waiter: object;
    cashier: object;
    dateClosed: Date;
    creationDate: Date;
    customer: object;
    productTransaction: object;
}
