 export class Products { 
    id: string; 
    name: string; 
    code: string;
    categories: object;
    price: number;
    availableQuantity: number;
    forsale: boolean;
}
