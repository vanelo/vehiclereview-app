import { Pipe, PipeTransform } from '@angular/core';
import { Products } from '../../models/Products';

@Pipe({
  name: 'productfilter',
  pure: false
})
export class ProductsFilterPipe implements PipeTransform {
  transform(items: any[], filter: Products): any {
      if (!items || !filter) {
        return items;
      }
      if(!filter.categories){
        return items.filter(product => product.name.toUpperCase().indexOf(filter.name) !== -1);
      }
      return items.filter(product => {
        if(product.categories && product.categories.length > 0){
          for(let thisCategory of product.categories){
            if(JSON.stringify(thisCategory) == JSON.stringify(filter.categories)){
              return true;
            }
          }
        }
        return false;
      });
  }
}
