import { Pipe, PipeTransform } from '@angular/core';
import { Orders } from '../../models/Orders';

@Pipe({
  name: 'orderfilter',
  pure: false
})
export class OrdersFilterPipe implements PipeTransform {
    transform(items: any[], filter: Orders): any {
        if (!items || !filter) {
            return items;
        }
        if(filter.status == "all"){
            return items;
        }
        console.log("filter.status: "+filter.status);
        return items.filter(order => order.status == filter.status);
    }
}
