import { ToastController } from 'ionic-angular';
import { Injectable } from '@angular/core';

@Injectable()
export class NotificationTools {

	shortDuration:number = 4000;
	longDuration:number = 7000;

	constructor(private toastCtrl: ToastController) {

	}

	simpleToast(position:string, msg:string) {
		let toast = this.toastCtrl.create({
			message: msg,
			duration: this.shortDuration,
			position: position
		});
		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});
		toast.present();
	}
	toastWithCloseButton(msg:string) {
		let toast = this.toastCtrl.create({
			message: msg,
			showCloseButton: true,
			closeButtonText: 'Ok'
		});
		toast.present();
	}
	errorMessage(msg:string){
		let toast = this.toastCtrl.create({
			message: msg,
			duration: this.shortDuration,
			position: 'top',
			cssClass: "errorMessage"
		});
		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});
		toast.present();
	}
	infoMessage(msg:string){
		let toast = this.toastCtrl.create({
			message: msg,
			duration: this.shortDuration,
			position: 'top',
			cssClass: "infoMessage"
		});
		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});
		toast.present();
	}
}
