import { Component } from '@angular/core';

import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DriverServiceProvider } from '../../providers/driver-service/driver-service';

import { VehicleReviewPage } from '../vehicleReview/vehicleReview';

import { NotificationTools } from '../../shared/notification/notification-tools';

@Component({
	selector: 'page-vehicleFinder',
	templateUrl: 'vehicleFinder.html'
})
export class VehicleFinderPage {

	/* Scope variable */
	vehicles = [];
	driverDni = "";
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public alertCtrl: AlertController,
		public driverService: DriverServiceProvider,
		public notification: NotificationTools
	) {	}

	findVehicle(){
		if(this.driverDni.length >= 6){
			this.driverService.getVehicleByDriver(this.driverDni, (error, vehicles) => {
				if(!error){
					this.vehicles = vehicles;
				}else{
					this.notification.errorMessage("Error en la busqueda de vehiculos");
				}
			});
		}
	}
	openVehicleReviewPage(vehicleSelected){
		this.navCtrl.setRoot(VehicleReviewPage, {vehicleSelected: vehicleSelected});
	}

	test(text){
		alert(text);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad VehicleFinderPage');
	}

}
