import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { DatePicker } from '@ionic-native/date-picker';
import { VehicleReviewServiceProvider } from '../../providers/vehiclereview-service/vehiclereview-service';
import { VehicleFinderPage } from '../vehicleFinder/vehicleFinder';

import { NotificationTools } from '../../shared/notification/notification-tools';

@IonicPage()
@Component({
  selector: 'page-vehicleReview',
  templateUrl: 'vehicleReview.html',
})
export class VehicleReviewPage {
	// Form items
	review = {
		headlightsGlass : { 							// Faros
			frontRight: false,
			frontLeft: false,
			rearRight: false,
			rearLeft: false
		},
		headlights : { 										// Luces
			frontRight: false,
			frontLeft: false,
			rearRight: false,
			rearLeft: false
		},
		highBeam : { 											// Luz alta
			frontRight: false,
			frontLeft: false
		},
		fogLamps : { 											// Busca huellas
			frontRight: false,
			frontLeft: false
		},
		turnSignals : { 									//Luces de giro
			frontRight: false,
			frontLeft: false,
			rearRight: false,
			rearLeft: false
		},
		stopLamps : { 										// Luces de freno
			rearLeft: false,
			rearRight: false
		},
		licensePlateLamp : false, 				// Luz de chapa
		recoilLamp : false, 							// Luz de retroceso
		tires : false, 										// Neumaticos
		windows : false, 									// ventanas de puertas
		bumpers : false, 									// Parachoques
		mirrors : false, 									// Espejos retrovisores
		frontEnd : false, 								// Tren delantero
		licensePlate : false, 						// Chapa
		sheetAndPaint : false, 						// Chaperia y pintura
		brakePedal : false,								// Frenos de pedal
		emergencyBrake : false, 					//Freno de emergencia
		steeringMechanism : false, 				// Mecanismo de direccion
		windshields : false, 							// Parabrisas
		windshieldsWasher : false, 				// Limpia parabrisas
		silencerAndExhaustGasket : false, //Silenciador y sistema de escape
		airConditionig : false, 					// Aire acondicionado
		radioPlayer : false, 							// sistema de radio
		seatBelt : false, 								// Cinturones de seguridad
		seatAdjustment : false, 					// Ajuste de asientos delateros
		doors : false, 										// Puertas
		horn : false, 										// Bocina
		speedometer : false,						 	// Velocimetro
		seatCover : false, 								// Tapizado
		cleaning : false, 								// Limpieza
		airbags : false,
		extinguisher : "2019-12-12",			// Extintor
		beacon : false, 									// Baliza
		jack : false, 										// Gato
		spareTire : false, 								// Rueda de auxiio
		tools : false,	 										// Herramientas
		insurance : false, 								// Seguro
		authorization : "TITULAR", 				// Autorizacion
		aditionalData : ""
	}

  vehicleSelected = {
	  model: "",
	  year: "",
	  brand: "",
	  review: ""
  };
	
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
	private vehicleReviewService: VehicleReviewServiceProvider,
	public notification: NotificationTools
	// private datePicker: DatePicker
  ) { }

	// showDatePicker(){
	// 	// this.datePicker.show({
	// 	// 	date: new Date(),
	// 	// 	mode: 'date',
	// 	// 	androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
	// 	// }).then(
	// 	// 	date => console.log('Got date: ', date),
	// 	// 	err => console.log('Error occurred while getting date: ', err)
	// 	// );
	// }

	getReviewData(idReview: String){
		this.vehicleReviewService.getReviewById(idReview, (error, reviewData) => {
			if(!error){
				console.log(reviewData);
				if(reviewData){
					this.review = reviewData;
				}
			}
		});
	}
	saveReview(){
		this.vehicleReviewService.saveReview(this.vehicleSelected.review, this.review, (error) => {
			if(!error){
				this.notification.infoMessage("Los datos se guardaron correctamente");
				alert(JSON.stringify(this.review));
				this.navCtrl.setRoot(VehicleFinderPage);
			}else{
				this.notification.errorMessage("Error al guardar los datos de revision")
			}
		});
	}
  ionViewDidLoad() {
		console.log('ionViewDidLoad VehicleReviewPage');
		this.vehicleSelected = this.navParams.get("vehicleSelected");
		if(this.vehicleSelected.review){
			this.getReviewData(this.vehicleSelected.review);
		}
		console.log(JSON.stringify(this.vehicleSelected));
  }

}
