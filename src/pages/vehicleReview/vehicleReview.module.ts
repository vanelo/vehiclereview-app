import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleReviewPage } from './vehicleReview';

@NgModule({
  declarations: [
    VehicleReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(VehicleReviewPage),
  ],
})
export class OrdersPageModule {}
