import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { LoginServiceProvider } from '../../providers/login-service/login-service';
import { VehicleFinderPage } from '../vehicleFinder/vehicleFinder';

import { NotificationTools } from '../../shared/notification/notification-tools';

@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage {
	// scope varaibles
	username;
	password;

	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams, 
		private loginService: LoginServiceProvider,
		public alertCtrl: AlertController,
		public notification: NotificationTools
	) { }

	signInUser(){
		let params = {
			username : this.username,
			password : this.password
		};
		this.loginService.doLogin(params, (error, thisData)=>{
			if(!error){
				if(thisData.result=="OK"){
					this.navCtrl.setRoot(VehicleFinderPage);
				}else{
					this.notification.errorMessage("Verifique su usuario y contraseña");
				}
			}else{
				this.notification.errorMessage("Error, compruebe su conexión e intente nuevamente");
			}
		})
	}
	ionViewDidLoad() {
		console.log('ionViewDidLoad LoginPage');
	}
}