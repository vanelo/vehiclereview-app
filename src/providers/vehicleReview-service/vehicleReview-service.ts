import { HttpClient,HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class VehicleReviewServiceProvider {
  //serverUrl:string = "http:/192.168.0.111:3000";
  headers = new  HttpHeaders({
    "Content-Type":  "application/json",
    "Access-Control-Allow-Origin": "*"
  });

  reviewData: Object;
  constructor(
    public http: HttpClient
  ) {
    console.log("Hello VehicleReviewServiceProvider Provider");
  }

  getReviewById(reviewId, cb){
    if(reviewId.length <= 0){ 
      cb(null, null); return;
    }
    this.reviewData = {
      headlightsGlass : {
        frontRight: true,
        frontLeft: false,
        rearRight: false,
        rearLeft: false
      },
      headlights : {
        frontRight: false,
        frontLeft: true,
        rearRight: false,
        rearLeft: false
      },
      highBeam : {
        frontRight: false,
        frontLeft: false
      },
      fogLamps : {
        frontRight: false,
        frontLeft: false
      },
      turnSignals : {
        frontRight: false,
        frontLeft: true,
        rearRight: true,
        rearLeft: false
      },
      stopLamps : {
        rearLeft: false,
        rearRight: false
      },
      licensePlateLamp : false,
      recoilLamp : false,
      tires : true,
      windows : false,
      bumpers : false,
      mirrors : false,
      frontEnd : false,
      licensePlate : false,
      sheetAndPaint : false,
      brakePedal : false,
      emergencyBrake : false,
      steeringMechanism : false,
      windshields : false,
      windshieldsWasher : false,
      silencerAndExhaustGasket : false,
      airConditionig : true,
      radioPlayer : false,
      seatBelt : false,
      seatAdjustment : false,
      doors : false,
      horn : false,
      speedometer : false,
      seatCover : false,
      cleaning : false,
      airbags : false,
      extinguisher : "12/12/2019",
      beacon : false,
      jack : false,
      spareTire : false,
      tools : false,
      insurance : false,
      authorization : "TITULAR",
      aditionalData : "A este vehiculo le falta una pulida nada más"
    }
    console.log(this.reviewData);

    cb(null, this.reviewData);

    // this.http.get("/api/v1/order?cardNumber="+cardNumber+"?status="+status, {headers: this.headers})
    // .subscribe(
    //   data=> {
    //     console.log("getOrderByCard data: "+data);
    //     cb(null, data);
    //   },
    //   error=> {
    //     console.log("getOrderByCard error: "+ JSON.stringify(error));
    //     cb(error, null);
    //   }
    // );
  }
  saveReview(reviewId, review, cb){
    // Save or update Review
    cb(null);
  }
}
