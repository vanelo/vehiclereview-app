import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class LoginServiceProvider {
  //serverUrl:string = "http:/192.168.0.111:3000";
  headers = new  HttpHeaders({
    "Content-Type":  "application/json",
    "Access-Control-Allow-Origin": "*"
  });  
  constructor(public http: HttpClient) {
    console.log("Hello LoginServiceProvider Provider");
  }
  doLogin(loginParams, cb){
    let params = JSON.stringify(loginParams);
    var testData = {
      result: "OK"
    };
    cb(null, testData);
    // this.http.post("/api/v1/employee/login", params, {headers: this.headers})
    // .subscribe(
    //   data => {
    //     console.log("login data: "+JSON.stringify(data));
    //     cb(null, data);
    //   },
    //   error=> {
    //     console.log("login error: "+ JSON.stringify(error));
    //     cb(error, null);
    //   }
    // );
  }

  logout(cb){
    cb(null, null);
    // this.http.post("/api/v1/employee/logout",{}, {headers: this.headers})
    // .subscribe(
    //   data=> {
    //     console.log("login data: "+JSON.stringify(data));
    //     cb(null, data);
    //   },
    //   error=> {
    //     console.log("login error: "+ JSON.stringify(error));
    //     cb(error, null);
    //   }
    // );
  }
}
