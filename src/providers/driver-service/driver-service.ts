import { HttpClient,HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class DriverServiceProvider {
  //serverUrl:string = "http:/192.168.0.111:3000";
  headers = new  HttpHeaders({
    "Content-Type":  "application/json",
    "Access-Control-Allow-Origin": "*"
  });

  constructor(
    public http: HttpClient
  ) {
    console.log("Hello DriverServiceProvider Provider");
  }

  getVehicleByDriver(dni, cb){
    const data = [
        { _id: "1", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "1" },
        { _id: "2", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "2" },
        { _id: "3", model: "Tucson", brand: "Hyundai", year: "2018", approbed: false },
        { _id: "4", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "4" },
        { _id: "5", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "5" },
        { _id: "6", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "6" },
        { _id: "7", model: "Tucson", brand: "Hyundai", year: "2018", approbed: false, review: "7" },
        { _id: "8", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "8" },
        { _id: "9", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "9" },
        { _id: "10", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "10" },
        { _id: "11", model: "Tucson", brand: "Hyundai", year: "2018", approbed: false },
        { _id: "12", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "11" },
        { _id: "13", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "12" },
        { _id: "14", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "13" },
        { _id: "15", model: "Tucson", brand: "Hyundai", year: "2018", approbed: false },
        { _id: "16", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "14" },
        { _id: "17", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "15" },
        { _id: "18", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "16" },
        { _id: "19", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "17" },
        { _id: "20", model: "Rav4", brand: "Toyota", year: "2018", approbed: true, review: "18" },
        { _id: "21", model: "Tucson", brand: "Hyundai", year: "2018", approbed: false }
      ];
    cb(null, data);
    // this.http.get("/api/v1/order?cardNumber="+cardNumber+"?status="+status, {headers: this.headers})
    // .subscribe(
    //   data=> {
    //     console.log("getOrderByCard data: "+data);
    //     cb(null, data);
    //   },
    //   error=> {
    //     console.log("getOrderByCard error: "+ JSON.stringify(error));
    //     cb(error, null);
    //   }
    // );
  }
}
