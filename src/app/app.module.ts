import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, LOCALE_ID, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { registerLocaleData } from '@angular/common';
import localeEsPY from '@angular/common/locales/es-PY';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { VehicleFinderPage } from '../pages/vehicleFinder/vehicleFinder';
import { VehicleReviewPage } from '../pages/vehicleReview/vehicleReview';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginServiceProvider } from '../providers/login-service/login-service';
import { DriverServiceProvider } from '../providers/driver-service/driver-service';
import { VehicleReviewServiceProvider } from '../providers/vehiclereview-service/vehiclereview-service';

import { NotificationTools } from '../shared/notification/notification-tools';

registerLocaleData(localeEsPY);

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    VehicleFinderPage,
    VehicleReviewPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    VehicleFinderPage,
    VehicleReviewPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: LOCALE_ID, useValue: 'es-PY'},
    LoginServiceProvider,
    DriverServiceProvider,
    VehicleReviewServiceProvider,
    NotificationTools
  ]
})
export class AppModule {}
