# Vehicle Review App #
Esta aplicacion es un cliente de MUV para hacer la revision de los vehiculos usando dispositivos moviles.
Puede ser compilado para utilizarse en los diferentes S.O. soportados por * Ionic *.
## Instalar dependencias ##
- Nodejs
- Android Studio
- Java (agregar entre las variables de entorno JAVA_HOME)
- Ionic: npm install -g ionic
- Cordova: npm i -g cordova

## Instalar plugins ##
npm install

## Reinstalar la plataforma ##
ionic cordova platform rm android

ionic cordova platform add android

## Para ejecutar la APP en modo debug ##

ionic cordova run android -l -c -s --debug

## Para ejecutar en modo produccion ##

ionic cordova run android --prod
 or
ionic cordova build android --prod